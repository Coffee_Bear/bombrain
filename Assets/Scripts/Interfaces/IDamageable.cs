
public interface IDamageable : ISpawnable
{
    public float health { get; set; }

    void Die();
}
