using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpawnPoint
{
    Vector3 position { get; set; }
    FactoryBase GetFactory();
}
