using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameField
{
    void Init();
    ISpawnPoint[] GetSpawnPoints();
}
