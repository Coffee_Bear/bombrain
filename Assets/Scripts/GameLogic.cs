using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLogic : MonoBehaviour
{
    [SerializeField]
    private int maxBombCount;
    private int currentBombCount;

    [SerializeField]
    private int maxCharacterCount;
    private int currentCharacterCount;

    private SpawnSystem _spawnSystem;
    private ExplosionSystem _explosionSystem;
    private MoveSystem _moveSystem;


    private void Start()
    {
        Init();
    }

    private void Update()
    {
        _moveSystem.Update(Time.deltaTime);
    }

    private void Init()
    {
        var gameField = FindGameFields()[0]; 
        gameField.Init();

        _spawnSystem = new SpawnSystem(gameField);
        _explosionSystem = new ExplosionSystem();
        _moveSystem = new MoveSystem();

        _explosionSystem.BombExploded += BombExplodedHandler;
        _explosionSystem.DamageableDied += CharacterDiedHandler;

        StartCoroutine(SpawnCharacters());
        StartCoroutine(SpawnBombs());
    }

    private IEnumerator SpawnCharacters()
    {
        while(true)
        {
            if (maxCharacterCount > currentCharacterCount)
            {
                var character = _spawnSystem.Spawn<Character>();
                _moveSystem.AddMoveable(character);
                _explosionSystem.AddDamageable(character);
                ++currentCharacterCount;
            }

            yield return new WaitForSeconds(1.2f);
        }
    }

    private IEnumerator SpawnBombs()
    {
        while (true)
        {
            if (maxBombCount > currentBombCount)
            {
                int randomValue = UnityEngine.Random.Range(0, 2);

                BombBase bomb;
                if (randomValue == 0)
                    bomb = _spawnSystem.Spawn<BombWithTimer>();
                else
                    bomb = _spawnSystem.Spawn<BombContact>();

                _explosionSystem.ActivateBomb(bomb);
                ++currentBombCount;
            }

            yield return new WaitForSeconds(0.8f);
        }
    }

    private List<IGameField> FindGameFields()
    {
        List<IGameField> gameFields = new List<IGameField>();
        List<GameObject> rootObjectsInScene = new List<GameObject>();  
        gameObject.scene.GetRootGameObjects(rootObjectsInScene);

        for (int i = 0; i < rootObjectsInScene.Count; i++)
        {
            IGameField[] additionalGameFields = rootObjectsInScene[i].GetComponentsInChildren<IGameField>(true);
            gameFields.AddRange(additionalGameFields);
        }

        return gameFields;
    }

    private void BombExplodedHandler(BombBase bomb)
    {
        _spawnSystem.Despawn(bomb);
        
        --currentBombCount;        
    }

    private void CharacterDiedHandler(IDamageable damageable)
    {
        _explosionSystem.RemoveDamageable(damageable);
        _spawnSystem.Despawn(damageable);

        if (damageable is IMoveable)
            _moveSystem.RemoveMoveable((IMoveable)damageable);
        
        --currentCharacterCount;
    }
}
