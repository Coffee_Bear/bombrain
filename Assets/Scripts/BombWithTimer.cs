using System;
using System.Collections;
using UnityEngine;

public class BombWithTimer : BombBase
{
    private BombWithTimerComponent _component;

    private Action<BombBase> _explosionCallback;

    public override float explosionRadius => _component.explosionRadius;
    public override float damage => _component.damage;

    public override SpawnedObject spawnedObject { get; protected set; }
    
    public BombWithTimer(SpawnedObject spawnedObject)
    {
        this.spawnedObject = spawnedObject;
        _component = spawnedObject.gameObject.GetComponent<BombWithTimerComponent>(); //���� ���������
    }

    public override void InitExplosion(Action<BombBase> callback)
    {
        _explosionCallback += callback;
        _component.StartCoroutine(ExplodeAfterTime());
    }

    private IEnumerator ExplodeAfterTime()
    {
        yield return new WaitForSeconds(_component.lifeTime);
        _component.Explode();
        _explosionCallback?.Invoke(this);
    }
}
