using System;
using UnityEngine;

[Serializable]
public class MazeSpawnPoint: ISpawnPoint
{
    [SerializeField]
    private FactoryBase factory;
    
    public Vector3 position { get; set; }

    public FactoryBase GetFactory() => factory;
}