using System;
using UnityEngine;

public class CharacterComponent : MonoBehaviour
{
    public event Action<CharacterComponent, Collision> ContactedWithSomething;

    public float health;
    public float speed;
    public Vector3 direction;

    public Rigidbody rigidbody;

    private void OnEnable()
    {
        RigidbodyUtilities.Reset(rigidbody);
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        ContactedWithSomething?.Invoke(this, collision);
    }

    public void Die()
    {
        Debug.Log("Character died!");
    }

}