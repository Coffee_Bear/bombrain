using System;
using UnityEngine;

public class BombContactComponent : MonoBehaviour
{
    public event Action ContactedWithSomething;

    public float health;
    public float explosionRadius;
    public int damage;

    public Rigidbody rigidbody;

    private void OnEnable()
    {
        RigidbodyUtilities.Reset(rigidbody);
    }

    public void Explode()
    {
        Debug.Log("Boom on contact!!!!!");
    }

    private void OnCollisionEnter(Collision collision)
    {
        ContactedWithSomething?.Invoke();
    }
}
