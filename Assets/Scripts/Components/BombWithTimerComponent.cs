using System;
using System.Collections;
using UnityEngine;

public class BombWithTimerComponent : MonoBehaviour
{
    public float health;
    public float explosionRadius;
    public int damage;
    public float lifeTime;

    public Rigidbody rigidbody;

    private void OnEnable()
    {
        RigidbodyUtilities.Reset(rigidbody);
    }

    public void Explode()
    {
        Debug.Log($"Boom after delay {lifeTime} seconds!!!!!");
    }
}
