using System;
using UnityEngine;

public abstract class BombBase: ISpawnable
{
    public abstract float explosionRadius { get; }
    public abstract float damage { get; }
    public abstract SpawnedObject spawnedObject { get; protected set; }

    public bool isDead { get; protected set; }

    public abstract void InitExplosion(Action<BombBase> callback);
}
