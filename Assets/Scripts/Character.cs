using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : IMoveable, IDamageable
{
    private CharacterComponent _component;

    private float _health;

    public float health
    {
        get => _health;
        set => _health = value;
    }

    public Vector3 position => spawnedObject.transform.position;

    public SpawnedObject spawnedObject { get; private set; }

    public bool isDead { get; private set; }

    public Character(SpawnedObject spawnedObject)
    {
        this.spawnedObject = spawnedObject;
        _component = spawnedObject.gameObject.GetComponent<CharacterComponent>(); //� ��� ���������
        _component.ContactedWithSomething += ChangeDirection;
        _health = _component.health;
    }

    public void Move(float deltaTime)
    {
        spawnedObject.transform.position += _component.speed * spawnedObject.transform.forward * deltaTime;
    }

    private void ChangeDirection(CharacterComponent component, Collision collision)
    {
        spawnedObject.transform.Rotate(Vector3.up, 45);
    }

    public void Die()
    {
        if (!isDead)
        {
            isDead = true;
            _component.Die();
        }
    }
}
