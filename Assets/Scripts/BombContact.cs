using System;

public class BombContact : BombBase
{
    private BombContactComponent _component;

    private Action<BombBase> _explosionCallback;

    public override float explosionRadius => _component.explosionRadius;
    public override float damage => _component.damage;

    public override SpawnedObject spawnedObject { get; protected set; }

    public BombContact(SpawnedObject spawnedObject)
    {
        this.spawnedObject = spawnedObject;
        _component = spawnedObject.gameObject.GetComponent<BombContactComponent>(); //��������� � �������
    }

    public override void InitExplosion(Action<BombBase> callback)
    {
        _explosionCallback += callback;
        _component.ContactedWithSomething += Explode;
    }

    private void Explode()
    {
        _component.ContactedWithSomething -= Explode;
        _component.Explode();
        _explosionCallback?.Invoke(this);        
    }
}
