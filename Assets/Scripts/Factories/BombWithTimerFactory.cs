using System;
using UnityEngine;

[CreateAssetMenu(fileName ="new BombWithTimerFactory", menuName = "Factories/BombWithTimerFactory" )]
public class BombWithTimerFactory : FactoryBase
{
    [SerializeField]
    private GameObject _prefab;
    public override GameObject prefab => _prefab;

    public override Type objectType => typeof(BombWithTimer);

    public override ISpawnable Create(SpawnedObject spawnedObject)
    {
        return new BombWithTimer(spawnedObject);
    }
}
