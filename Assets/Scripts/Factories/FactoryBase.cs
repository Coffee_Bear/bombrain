using System;
using UnityEngine;

public abstract class FactoryBase : ScriptableObject
{
    public abstract GameObject prefab { get; }

    public abstract Type objectType { get; }

    public abstract ISpawnable Create(SpawnedObject spawnedObject);
}
