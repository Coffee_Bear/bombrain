using System;
using UnityEngine;

[CreateAssetMenu(fileName ="new BombContactFactory", menuName = "Factories/BombContactFactory" )]
public class BombContactFactory : FactoryBase
{
    [SerializeField]
    private GameObject _prefab;
    public override GameObject prefab => _prefab;

    public override Type objectType => typeof(BombContact);

    public override ISpawnable Create(SpawnedObject spawnedObject)
    {
        return new BombContact(spawnedObject);
    }
}
