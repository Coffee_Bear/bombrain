using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new CharacterFactory", menuName = "Factories/CharacterFactory")]
public class CharacterFactory : FactoryBase
{
    [SerializeField]
    private GameObject _prefab;
    public override GameObject prefab => _prefab;

    public override Type objectType => typeof(Character);

    public override ISpawnable Create(SpawnedObject spawnedObject)
    {
        return new Character(spawnedObject);
    }
}
