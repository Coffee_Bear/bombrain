using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RigidbodyUtilities
{
    public static void Reset(Rigidbody rigidbody)
    {
        if (rigidbody != null)
        {
            rigidbody.velocity = new Vector3(0f, 0f, 0f);
            rigidbody.angularVelocity = new Vector3(0f, 0f, 0f);
        }
    }
}
