using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnedObject
{
    private int _instanceID;

    public GameObject gameObject { get; private set; }
    public Transform transform { get; private set; }

    public Vector3 position => transform.position;

    public SpawnedObject(GameObject gameObject)
    {
        this.gameObject = gameObject;
        transform = gameObject.transform;

        _instanceID = gameObject.GetInstanceID();
    }

    public int GetInstanceID() => _instanceID;    
}

public class ObjectPool : IDisposable
{
    private Dictionary<int, Queue<SpawnedObject>> cachedObjects;
    private Dictionary<int, int> cachedIds;
    private Transform poolParent;
    protected int index;

    public ObjectPool(Transform parent)
    {
        cachedObjects = new Dictionary<int, Queue<SpawnedObject>>();
        cachedIds = new Dictionary<int, int>();
        poolParent = parent;
    }

    public SpawnedObject Spawn(GameObject prefab, Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion), Transform parent = null)
    {
        index++;
        int key = prefab.GetInstanceID();
        Queue<SpawnedObject> queue;
        bool stacked = cachedObjects.TryGetValue(key, out queue);

        if (stacked && queue.Count > 0)
        {
            SpawnedObject pooledObject = queue.Dequeue();
            Transform transform = pooledObject.transform;

            if(parent != null)
                transform.SetParent(parent);

            transform.rotation = rotation;
            transform.position = position;

            pooledObject.gameObject.SetActive(true);

            return pooledObject;
        }

        if (!stacked)
            cachedObjects.Add(key, new Queue<SpawnedObject>());

        var createdInstance = Populate(prefab, position, rotation, parent);
        cachedIds.Add(createdInstance.GetInstanceID(), key);
        return createdInstance;
    }
    
    public void Despawn(SpawnedObject pooledObject)
    {
        index--;
        pooledObject.gameObject.SetActive(false);
        cachedObjects[cachedIds[pooledObject.GetInstanceID()]].Enqueue(pooledObject);
        
        if (poolParent != null)
            pooledObject.transform.SetParent(poolParent);
    }

    public void Dispose()
    {
        poolParent = null;
        cachedObjects.Clear();
        cachedIds.Clear();
    }

    private SpawnedObject Populate(GameObject prefab, Vector3 position = default(Vector3), Quaternion rotation = default(Quaternion), Transform parent = null)
    {
        GameObject instance = GameObject.Instantiate(prefab, position, rotation);
        if (parent != null)
            instance.transform.SetParent(parent);

        SpawnedObject pooledObject = new SpawnedObject(instance);
        pooledObject.transform.position = position;

        return pooledObject;
    }
}
