using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSystem
{
    private ObjectPool _objectPool;
    private Dictionary<Type, List<ISpawnPoint>> _spawnPoints;

    public SpawnSystem(IGameField gameField)
    {
        GameObject objectPoolGO = new GameObject("SpawnSystem ObjectPool");
        GameObject.DontDestroyOnLoad(objectPoolGO);
        _objectPool = new ObjectPool(objectPoolGO.transform);

        InitializeSpawnPoints(gameField);
    }

    private void InitializeSpawnPoints(IGameField gameField)
    {
        _spawnPoints = new Dictionary<Type, List<ISpawnPoint>>();

        var spawnPoints = gameField.GetSpawnPoints();
        foreach (var spawnPoint in spawnPoints)
        {
            var factory = spawnPoint.GetFactory();
            List<ISpawnPoint> spawnPointList;
            if (!_spawnPoints.ContainsKey(factory.objectType))
            {
                spawnPointList = new List<ISpawnPoint>();
                _spawnPoints.Add(factory.objectType, spawnPointList);
            }
            else
            {
                spawnPointList = _spawnPoints[factory.objectType];
            }

            spawnPointList.Add(spawnPoint);
        }
    }

    public T Spawn<T>() where T : class, ISpawnable
    {
        Type type = typeof(T);
        T instance = null;

        List<ISpawnPoint> spawnPontsList;
        if (_spawnPoints.TryGetValue(type, out spawnPontsList))
        {
            int randomIndex = UnityEngine.Random.Range(0, spawnPontsList.Count);
            var spawnPoint = spawnPontsList[randomIndex];
            var factory = spawnPoint.GetFactory();
            var spawnedObject = _objectPool.Spawn(factory.prefab, spawnPoint.position);
            instance = (T)factory.Create(spawnedObject);
        }

        return instance;
    }

    public void Despawn(ISpawnable spawnable)
    {
        _objectPool.Despawn(spawnable.spawnedObject);
    }
}
