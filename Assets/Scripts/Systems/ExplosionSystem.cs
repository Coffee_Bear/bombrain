using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSystem
{
    public event Action<IDamageable> DamageableDied;
    public event Action<BombBase> BombExploded;

    private LinkedList<IDamageable> _damageables = new LinkedList<IDamageable>();

    public void ActivateBomb(BombBase bomb)
    {
        bomb.InitExplosion(ExplodeHandler);
    }

    public void AddDamageable(IDamageable damagable)
    {
        _damageables.AddLast(damagable);
    }

    public void RemoveDamageable(IDamageable damageable)
    {
        _damageables.Remove(damageable);
    }

    private void ExplodeHandler(BombBase bomb)
    {
        List<IDamageable> deads = new List<IDamageable>();
        foreach (var damagable in _damageables)
        {
            if (IsDamageDone(bomb, damagable))
                damagable.health -= bomb.damage;

            if (damagable.health <= 0)
                deads.Add(damagable);
        }

        foreach (var dead in deads)
        {
            if (dead.spawnedObject.GetInstanceID() != bomb.spawnedObject.GetInstanceID())
                dead.Die();

            DamageableDied?.Invoke(dead);
        }

        BombExploded?.Invoke(bomb);
    }    

    private bool IsDamageDone(BombBase bomb, IDamageable damageable)
    {
        float distance = (damageable.spawnedObject.transform.position - bomb.spawnedObject.position).magnitude;
        if (distance > bomb.explosionRadius)
            return false;

        Vector3 direction = (damageable.spawnedObject.transform.position - bomb.spawnedObject.position).normalized;

        RaycastHit raycastHit;
        Physics.Raycast(bomb.spawnedObject.position, direction, out raycastHit, distance);

        return raycastHit.transform == damageable.spawnedObject.transform;
    }
}
