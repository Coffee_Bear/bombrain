using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSystem
{
    private LinkedList<IMoveable> _moveables = new LinkedList<IMoveable>();
    
    public void Update(float deltaTime)
    {
        foreach(var moveable in _moveables)        
            moveable.Move(deltaTime);        
    }

    public void AddMoveable(IMoveable moveable)
    {
        _moveables.AddLast(moveable);
    }

    public void RemoveMoveable(IMoveable moveable)
    {
        _moveables.Remove(moveable);
    }
}
